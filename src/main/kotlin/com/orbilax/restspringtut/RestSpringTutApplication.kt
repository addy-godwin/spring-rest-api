package com.orbilax.restspringtut

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class RestSpringTutApplication

fun main(args: Array<String>) {
    runApplication<RestSpringTutApplication>(*args)
}
